#!/usr/bin/env python3

from glob import glob
from pathlib import Path
import shlex
import shutil
import subprocess
import os

import typing as T


def quote_str(path: Path) -> str:
    return shlex.quote(str(path))


def run(*strings: T.List[str], **kwargs):
    # fmt: off
    args = [arg
        for s in strings
        for arg in shlex.split(s)
    ]
    # fmt: on
    subprocess.run(args, check=True, **kwargs)


IN_DIR = Path("orig")
OUT_DIR = Path("icons")
EXCLUDED = {"image-auto-adjust"}

def try_float(s, sentinel):
    try:
        return float(s)
    except ValueError:
        return sentinel

shutil.rmtree(OUT_DIR)

for category in ["actions"]:
    # Don't keep high-res icons, since they have fine detail intended for large icons,
    # not small icons on hidpi screens.
    # So far, the only "scalable" icons are from Tango, and work well enough at small sizes.
    for size in [16, 22, 32, "scalable"]:
        size_f = try_float(size, float("inf"))
        size = str(size)

        out_dir = Path(OUT_DIR, category)
        out_dir.mkdir(parents=True, exist_ok=True)

        def copy_to(path, out_size):
            suffix = ".svg" if out_size == "scalable" else ".png"
            out_path = out_dir / f"{path.stem}-{out_size}{suffix}"
            if path.suffix != suffix:
                assert path.suffix == ".svg", (path.suffix, ".svg")
                assert suffix == ".png", (suffix, ".png")
                run("magick convert -strip -background none", quote_str(path), quote_str(out_path))

            else:
                shutil.copy(path, out_path)

        dirs = [Path(category, size), Path(size, category), Path(f"{size}x{size}", category)]
        for dir in dirs:
            dir = IN_DIR / dir

            if not dir.exists():
                continue
            for path in dir.iterdir():
                if path.stem in EXCLUDED:
                    continue
                copy_to(path, size)

                # Some icons are available in 24 but not 32. Copy the largest size <= 32.
                if path.suffix == ".svg":
                    copy_to(path, "scalable")
